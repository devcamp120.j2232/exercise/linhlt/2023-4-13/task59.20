package com.devcamp.task5920jpaorder.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task5920jpaorder.model.COrder;

public interface IOrderRepository extends JpaRepository<COrder, Long>{
    
}
