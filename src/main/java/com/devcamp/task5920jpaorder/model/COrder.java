package com.devcamp.task5920jpaorder.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name = "orders")
public class COrder {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "order_code")
    private int orderCode;
    @Column(name = "size")
    private String size;
    @Column(name = "pizza_type")
    private String pizzaType;
    @Column(name = "drink")
    private String drink;
    @Column(name = "price")
    private long price;
    @Column(name = "customer_name")
    private String name;
    @Column(name = "phone")
    private String phone;
    @Column(name = "status")
    private String status;
    public COrder() {
    }
    public COrder(int orderCode, String size, String pizzaType, String drink, long price, String name, String phone,
            String status) {
        this.orderCode = orderCode;
        this.size = size;
        this.pizzaType = pizzaType;
        this.drink = drink;
        this.price = price;
        this.name = name;
        this.phone = phone;
        this.status = status;
    }
    public int getOrderCode() {
        return orderCode;
    }
    public void setOrderCode(int orderCode) {
        this.orderCode = orderCode;
    }
    public String getSize() {
        return size;
    }
    public void setSize(String size) {
        this.size = size;
    }
    public String getPizzaType() {
        return pizzaType;
    }
    public void setPizzaType(String pizzaType) {
        this.pizzaType = pizzaType;
    }
    public String getDrink() {
        return drink;
    }
    public void setDrink(String drink) {
        this.drink = drink;
    }
    public long getPrice() {
        return price;
    }
    public void setPrice(long price) {
        this.price = price;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    

}
